/* Language: Java (Android) */

package com.nebisoftware.nebisocial;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.net.URL;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.DownloadListener;
import android.webkit.JsResult;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.datatransport.runtime.TransportRuntime;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.messaging.FirebaseMessaging;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public WebView webView;

    public boolean doubleBackToExitPressedOnce = false;
    public boolean showtype = false;
    public DrawerLayout drawerLayout;
    public ActionBarDrawerToggle actionBarDrawerToggle;
    public Boolean showrn = false;
    public NavigationView nv;
    public ConstraintLayout pfpimg;
    public Context context;
    public FloatingActionButton addcontent;

    public static class AppRater {
        private final static String APP_TITLE = "NebiSocial";// App Name
        private final static String APP_PNAME = "com.nebisoftware.nebisocial";// Package Name

        private final static int DAYS_UNTIL_PROMPT = 3;//Min number of days
        private final static int LAUNCHES_UNTIL_PROMPT = 3;//Min number of launches

        public static void app_launched(Context mContext) {
            SharedPreferences prefs = mContext.getSharedPreferences("apprater", 0);
            if (prefs.getBoolean("dontshowagain", false)) { return ; }

            SharedPreferences.Editor editor = prefs.edit();

            // Increment launch counter
            long launch_count = prefs.getLong("launch_count", 0) + 1;
            editor.putLong("launch_count", launch_count);

            // Get date of first launch
            Long date_firstLaunch = prefs.getLong("date_firstlaunch", 0);
            if (date_firstLaunch == 0) {
                date_firstLaunch = System.currentTimeMillis();
                editor.putLong("date_firstlaunch", date_firstLaunch);
            }

            // Wait at least n days before opening
            if (launch_count >= LAUNCHES_UNTIL_PROMPT) {
                if (System.currentTimeMillis() >= date_firstLaunch +
                        (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000)) {
                    showRateDialog(mContext, editor);
                }
            }

            editor.commit();
        }

        public static void showRateDialog(final Context mContext, final SharedPreferences.Editor editor) {
            final Dialog dialog = new Dialog(mContext);
            dialog.setTitle("Rate " + APP_TITLE);

            LinearLayout ll = new LinearLayout(mContext);
            ll.setOrientation(LinearLayout.VERTICAL);

            TextView tv = new TextView(mContext);
            tv.setText("If you enjoy using " + APP_TITLE + ", please take a moment to rate it. Thanks for your support!");
            tv.setWidth(240);
            tv.setPadding(4, 0, 4, 10);
            ll.addView(tv);

            Button b1 = new Button(mContext);
            b1.setText("Rate " + APP_TITLE);
            b1.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + APP_PNAME)));
                    dialog.dismiss();
                }
            });
            ll.addView(b1);

            Button b2 = new Button(mContext);
            b2.setText("Remind me later");
            b2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            ll.addView(b2);

            Button b3 = new Button(mContext);
            b3.setText("No, thanks");
            b3.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (editor != null) {
                        editor.putBoolean("dontshowagain", true);
                        editor.commit();
                    }
                    dialog.dismiss();
                }
            });
            ll.addView(b3);

            dialog.setContentView(ll);
            dialog.show();
        }
    }

    private ViewTreeObserver.OnScrollChangedListener mOnScrollChangedListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppRater.app_launched(this);
        addcontent = findViewById(R.id.addcontent);
        addcontent.hide();
        addcontent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Intent intent=new Intent(MainActivity.this,AddContentActivity.class);
                //startActivity(intent);
                if (showtype == false){
                    webView.evaluateJavascript("document.getElementsByClassName('panel')[0].style = 'display: block;';", null);
                    showtype = true;
                }
                else {
                    showtype = false;
                    webView.evaluateJavascript("document.getElementsByClassName('panel')[0].style = 'display: none;';", null);
                }

            }
        });
        getSupportActionBar().setTitle(R.string.loading);
        FirebaseMessaging.getInstance().subscribeToTopic("notifications");
        pfpimg = findViewById(R.id.nav_header);
        context = getBaseContext();
        webView = (WebView) findViewById(R.id.webView1);
        webView.setVisibility(View.GONE);
        initWebView(webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setSaveFormData(true);
        webView.getSettings().setUserAgentString("Mozilla/5.0 (Android; Phone; rv:85.0) Gecko/20100101 Firefox/85.0");

        webView.loadUrl("https://social.nebisoftware.com"); // TODO input your url
        webView.clearHistory();

        drawerLayout = findViewById(R.id.my_drawer_layout);
        nv = findViewById(R.id.navView);
        nv.setNavigationItemSelectedListener(this);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,
                drawerLayout,
                R.string.open,
                R.string.close);

        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);


        NavigationView navigationView = (NavigationView) findViewById(R.id.navView);drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        webView.getViewTreeObserver().addOnScrollChangedListener(mOnScrollChangedListener =
                new ViewTreeObserver.OnScrollChangedListener() {
                    @Override
                    public void onScrollChanged() {
                        if (webView.getScrollY() == 0)
                            getSupportActionBar().setElevation(0);
                        else
                            getSupportActionBar().setElevation(16);

                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.my_drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(webView.canGoBack()) {
                webView.goBack();
            }
            else
            {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, R.string.backerr, Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce=false;
                    }
                }, 2000);
            }
        }
    }

    private final static Object methodInvoke(Object obj, String method, Class<?>[] parameterTypes, Object[] args) {
        try {
            Method m = obj.getClass().getMethod(method, new Class[] { boolean.class });
            m.invoke(obj, args);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private void initWebView(WebView webView) {

        WebSettings settings = webView.getSettings();

        settings.setJavaScriptEnabled(true);
        settings.setAllowFileAccess(true);
        settings.setDomStorageEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setSupportZoom(false);
        // settings.setPluginsEnabled(true);
        methodInvoke(settings, "setPluginsEnabled", new Class[] { boolean.class }, new Object[] { true });
        // settings.setPluginState(PluginState.ON);
        methodInvoke(settings, "setPluginState", new Class[] { PluginState.class }, new Object[] { PluginState.ON });
        // settings.setPluginsEnabled(true);
        methodInvoke(settings, "setPluginsEnabled", new Class[] { boolean.class }, new Object[] { true });
        // settings.setAllowUniversalAccessFromFileURLs(true);
        methodInvoke(settings, "setAllowUniversalAccessFromFileURLs", new Class[] { boolean.class }, new Object[] { true });
        // settings.setAllowFileAccessFromFileURLs(true);
        methodInvoke(settings, "setAllowFileAccessFromFileURLs", new Class[] { boolean.class }, new Object[] { true });

        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.clearHistory();
        webView.clearFormData();
        webView.clearCache(true);

        webView.setWebViewClient(new MyAppWebViewClient());

        webView.setWebChromeClient(new MyWebChromeClient());

        webView.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
    }

    UploadHandler mUploadHandler;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (requestCode == Controller.FILE_SELECTED) {
            // Chose a file from the file picker.
            if (mUploadHandler != null) {
                mUploadHandler.onResult(resultCode, intent);
            }
        }

        super.onActivityResult(requestCode, resultCode, intent);
    }

    public void ClickDirMenu(String id) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            webView.evaluateJavascript("document.getElementsByClassName('top-menu-item')[" + id + "].getElementsByTagName('a')[0].click();", null);
        } else {
            webView.loadUrl("javascript:" + "document.getElementsByClassName('top-menu-item')[" + id + "].getElementsByTagName('a')[0].click();");
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menubtn1:
                ClickDirMenu("0");
                break;
            case R.id.menubtn2:
                ClickDirMenu("1");
                break;
            case R.id.menubtn3:
                ClickDirMenu("2");
                break;
            case R.id.menubtn4:
                ClickDirMenu("3");
                break;
            case R.id.menubtn6:
                ClickDirMenu("4");
                break;
            case R.id.menubtn8:
                ClickDirMenu("5");
                break;

            case R.id.share_curr_url:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBodyText =  webView.getUrl();
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,"Subject here");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBodyText);
                startActivity(Intent.createChooser(sharingIntent, "Sharing Option"));
                return true;

            case R.id.search_ns:
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    webView.evaluateJavascript("document.getElementById('search-menu').click();", null);
                } else {
                    webView.loadUrl("javascript:" + "document.getElementById('search-menu').click();");
                }
                break;
            case R.id.create_space:
                if (showrn == false) {
                    showrn = true;
                    webView.evaluateJavascript("document.getElementById('space-menu-dropdown').style.display='block';", null);
                } else {
                    showrn = false;
                    webView.evaluateJavascript("document.getElementById('space-menu-dropdown').style.display='none';", null);
                }
                break;
            case R.id.show_rn:
                if (showrn == false) {
                    showrn = true;
                    webView.evaluateJavascript("document.getElementById('panel-activities').parentNode.style = 'transition-property: margin-left; transition-duration: 500ms;display: block; position: fixed; top: 0px; width: 100%; height: 100%; background: white none repeat scroll 0% 0%; margin-left: 0%; overflow-y: auto; z-index: 997; padding: 107px 5px 5px; transform: translate3d(0px, 0px, 0px);';", null);
                } else {
                    showrn = false;
                    webView.evaluateJavascript("document.getElementById('panel-activities').parentNode.style = 'transition-property: margin-left; transition-duration: 500ms; display: block; position: fixed; top: 0px; width: 100%; height: 100%; background: white none repeat scroll 0% 0%; margin-left: 100%; overflow-y: auto; z-index: 997; padding: 107px 5px 5px; transform: translate3d(0px, 0px, 0px);';", null);
                }
                break;

            case R.id.profile_btn:
                webView.evaluateJavascript("document.getElementsByClassName('account')[0].getElementsByTagName('ul')[0].getElementsByTagName('li')[0].getElementsByTagName('a')[0].click()", null);
                break;
            case R.id.prof_settings:
                webView.evaluateJavascript("document.getElementsByClassName('account')[0].getElementsByTagName('ul')[0].getElementsByTagName('li')[1].getElementsByTagName('a')[0].click()", null);
                break;
            case R.id.logout:
                webView.evaluateJavascript("document.getElementsByClassName('account')[0].getElementsByTagName('ul')[0].getElementsByTagName('li')[3].getElementsByTagName('a')[0].click()", null);
                break;
            case R.id.notify_btn:
                webView.evaluateJavascript("document.getElementById('dropdown-notifications').getElementsByTagName('li')[4].getElementsByTagName('a')[0].click();", null);
                break;
            case R.id.rate:
                String APP_PNAME = "com.nebisoftware.nebisocial";// Package Name
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + APP_PNAME)));
                break;

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.my_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void HideTopBar(WebView view){
            view.evaluateJavascript("document.getElementById('topbar-second').style='box-shadow: none !important; top: -50px !important;';", null);
            view.evaluateJavascript("document.getElementsByClassName('container')[1].style.boxShadow = 'none';", null);
            view.evaluateJavascript("document.getElementsByTagName('body')[0].style='padding-top: 0px !important;';", null);
            view.evaluateJavascript("document.getElementById('topbar-first').style='display:none;';", null);
    }


    public static String deparameterize(String uri) {
        int i = uri.lastIndexOf('?');
        if (i == -1) {
            return uri;
        }

        return uri.substring(0, i);
    }

    public void LoadPfp() {

        String pfpscr = "document.getElementById('user-account-image').getElementsByTagName('img')[0].src;";
        String profslogan = "document.getElementsByClassName('user-title pull-left hidden-xs')[0].getElementsByTagName('span')[0].innerHTML;";
        String profstr = "document.getElementsByClassName('user-title pull-left hidden-xs')[0].getElementsByTagName('strong')[0].innerHTML;";
        webView.evaluateJavascript(profstr,
                new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String html) {
                        JsonReader reader = new JsonReader(new StringReader(html));
                        reader.setLenient(true);
                        try {
                            if (reader.peek() != JsonToken.NULL) {
                                if (reader.peek() == JsonToken.STRING) {
                                    String msg = reader.nextString();
                                    if (msg != null) {
                                        String username = msg;
                                        TextView textElement = (TextView) findViewById(R.id.usernamestr);
                                        try {
                                            textElement.setText(msg);
                                        }
                                        catch (Exception e) {

                                        }
                                    }
                                }
                            }
                        } catch (IOException e) {
                            Log.e("TAG", "MainActivity: IOException", e);
                        } finally {
                            try {
                                reader.close();
                            } catch (IOException e) {
                            }
                        }
                    }
                });
        webView.evaluateJavascript(profslogan,
                new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String html) {
                        JsonReader reader = new JsonReader(new StringReader(html));
                        reader.setLenient(true);
                        try {
                            if (reader.peek() != JsonToken.NULL) {
                                if (reader.peek() == JsonToken.STRING) {
                                    String msg = reader.nextString();
                                    if (msg != null) {
                                        String username = msg;
                                        TextView textElement = (TextView) findViewById(R.id.userslogan);
                                        try {
                                            textElement.setText(msg);
                                        }
                                        catch (Exception e) {

                                        }
                                    }
                                    else if (msg == "") {
                                        TextView textElement = (TextView) findViewById(R.id.userslogan);
                                        textElement.setText(R.string.emptyslogan);
                                    }
                                }
                            }
                        } catch (IOException e) {
                            Log.e("TAG", "MainActivity: IOException", e);
                        } finally {
                            try {
                                reader.close();
                            } catch (IOException e) {
                            }
                        }
                    }
                });
        webView.evaluateJavascript(pfpscr,
                new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String html) {
                        JsonReader reader = new JsonReader(new StringReader(html));
                        reader.setLenient(true);
                        try {
                            if (reader.peek() != JsonToken.NULL) {
                                if (reader.peek() == JsonToken.STRING) {
                                    String msg = reader.nextString();
                                    if (msg != null) {
                                        String uriofimg = msg;
                                        WebView web = (WebView) findViewById(R.id.pfpview);
                                        try {
                                            web.loadDataWithBaseURL(null, "<html><body style='margin:0;'>" +
                                                    "<img style='margin:16px; border-radius: 100%; box-shadow:0 10px 15px -3px rgba(0,0,0,.07),0 4px 6px -2px rgba(0,0,0,.05);" +
                                                    "background-color: #ff5f5f;' src='" + uriofimg + "' width='64' height='64'/>" +
                                                    "<div style='background-color: #f5f5f5;filter: blur(2px);" +
                                                    "opacity: 0.25;width: 100% !important;top:-56; z-index: -1; height: 200px !important;" +
                                                    "background-image:url("+uriofimg+");background-repeat: no-repeat;position:absolute;background-size: cover;'>" +
                                                    "<div>" +
                                                    "</body></html>", "text/html", "utf-8", null);
                                        }
                                        catch (Exception e) {

                                        }
                                    }
                                }
                            }
                        } catch (IOException e) {
                            Log.e("TAG", "MainActivity: IOException", e);
                        } finally {
                            try {
                                reader.close();
                            } catch (IOException e) {
                            }
                        }
                    }
                });
    }

    public class MyAppWebViewClient extends WebViewClient {

        public void onPageFinished(WebView view, String url) {
            webView.evaluateJavascript("document.getElementById('space-menu-dropdown').style.display='none';", null);
            showrn = false;
            showtype = false;
            super.onPageFinished(view, url);

            view.evaluateJavascript("document.getElementsByTagName('head')[0].innerHTML= '<meta name='viewport' content='width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no'>' + document.getElementsByTagName('head')[0].innerHTML;", null);

            view.setVisibility(View.VISIBLE);

            if(view.getUrl().contains("https://social.nebisoftware.com/index.php?r=user%2Fauth%2Flogin")){
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                WebView web = (WebView) findViewById(R.id.pfpview);
                try {
                    TextView textElement = (TextView) findViewById(R.id.usernamestr);
                    textElement.setText(R.string.app_name);
                    TextView textElement2 = (TextView) findViewById(R.id.userslogan);
                    textElement2.setText(R.string.plzlogin);
                    String uriofimg = "https://social.nebisoftware.com/uploads/profile_image/38ecc7de-f3cf-49d7-9d62-b737c7b88da6.jpg?m=1635702221";
                    web.loadDataWithBaseURL(null, "<html><body style='margin:0;'>" +
                            "<img style='margin:16px; border-radius: 100%; box-shadow:0 10px 15px -3px rgba(0,0,0,.07),0 4px 6px -2px rgba(0,0,0,.05);" +
                            "background-color: #ff5f5f;' src='" + uriofimg + "' width='64' height='64'/>" +
                            "<div style='background-color: #f5f5f5;filter: blur(2px);" +
                            "opacity: 0.25;width: 100% !important;top:-56; z-index: -1; height: 200px !important;" +
                            "background-image:url("+uriofimg+");background-repeat: no-repeat;position:absolute;background-size: cover;'>" +
                            "<div>" +
                            "</body></html>", "text/html", "utf-8", null);
                }
                catch (Exception e) {

                }
            }
            else {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }

            if(view.getUrl().contains("https://social.nebisoftware.com/index.php?r=dashboard%2Fdashboard")){
                view.evaluateJavascript("document.getElementsByClassName('panel')[0].style = 'display: none;';", null);
                view.evaluateJavascript("document.getElementsByClassName('wallFilterPanel')[0].style = 'margin: 0 !important;';", null);
                view.evaluateJavascript("document.getElementsByClassName('wall-stream-filter-root')[0].style = 'display: none;';", null);
                addcontent.show();
            }
            else {
                addcontent.hide();
            }

            // Load profile picture and username
            if (view.getUrl().contains("jitsi-meet")){
                webView.evaluateJavascript("document.getElementById('jitsiConferenceFrame0').style = 'height:100vh !important;width: 100%; border: 0px none;';", null);
                webView.evaluateJavascript("document.getElementById('jitsiModal').style = 'background-color: transparent; height:100vh !important;';",null);
            } else {
                LoadPfp();
            }

            String title = view.getTitle().replace(" - NebiSocial", "");
            if (view.getTitle().contains("Unable to connect")){
                getSupportActionBar().setTitle(R.string.nonet);
                view.evaluateJavascript("document.getElementsByTagName('html')[0].style='background: rgb(245, 245, 245) none repeat scroll 0% 0%; color: rgb(53, 53, 53);';", null);
                view.evaluateJavascript("document.getElementById('content').style='background: rgb(245, 245, 245) none repeat scroll 0% 0% !important; color: rgb(53, 53, 53);';", null);
                view.evaluateJavascript("document.getElementsByTagName('a')[0].style.color='#ff5f5f';", null);
            } else {
                getSupportActionBar().setTitle(title);
            }

            // Hide user menu
            view.evaluateJavascript("document.getElementById('account-dropdown-link').style = 'display: none;';", null);

            // Remove comment ("//" at beginning of line) to hide second topbar at NebiSocial.
            HideTopBar(view);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if(Uri.parse(url).getHost().endsWith("social.nebisoftware.com")) {
                return false;
            }
            else if (Uri.parse(url).getHost().contains("google.com")) {
                return false;
            }
            else if (Uri.parse(url).getHost().endsWith("discord.com")) {
                return false;
            }
            else {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                view.getContext().startActivity(intent);
                return true;
            }
        }
    }

    class MyWebChromeClient extends WebChromeClient {
        public MyWebChromeClient() {

        }


        @Override
        public void onPermissionRequest(final PermissionRequest request) {
            if (webView.getUrl().contains("jitsi-meet")){
                webView.evaluateJavascript("document.getElementById('jitsiConferenceFrame0').style = 'height:100vh !important;width: 100%; border: 0px none;';", null);
                webView.evaluateJavascript("document.getElementById('jitsiModal').style = 'background-color: transparent; height:100vh !important;';",null);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                requestPermissions(request.getResources(), 0);
                request.grant(request.getResources());
            }
        }

        private String getTitleFromUrl(String url) {
            String title = url;
            try {
                URL urlObj = new URL(url);
                String host = urlObj.getHost();
                if (host != null && !host.isEmpty()) {
                    return urlObj.getProtocol() + "://" + host;
                }
                if (url.startsWith("file:")) {
                    String fileName = urlObj.getFile();
                    if (fileName != null && !fileName.isEmpty()) {
                        return fileName;
                    }
                }
            } catch (Exception e) {
                // ignore
            }

            return title;
        }

        @Override
        public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
            String newTitle = getTitleFromUrl(url);

            new AlertDialog.Builder(MainActivity.this).setTitle(newTitle).setMessage(message).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    result.confirm();
                }
            }).setCancelable(false).create().show();
            return true;
            // return super.onJsAlert(view, url, message, result);
        }

        @Override
        public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {

            String newTitle = getTitleFromUrl(url);

            new AlertDialog.Builder(MainActivity.this).setTitle(newTitle).setMessage(message).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    result.confirm();
                }
            }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    result.cancel();
                }
            }).setCancelable(false).create().show();
            return true;

            // return super.onJsConfirm(view, url, message, result);
        }

        // Android 2.x
        public void openFileChooser(ValueCallback<Uri> uploadMsg) {
            openFileChooser(uploadMsg, "");
        }

        // Android 3.0
        public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
            openFileChooser(uploadMsg, "", "filesystem");
        }

        // Android 4.1
        public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
            mUploadHandler = new UploadHandler(new Controller());
            mUploadHandler.openFileChooser(uploadMsg, acceptType, capture);
        }

        // Android 4.4, 4.4.1, 4.4.2
        // openFileChooser function is not called on Android 4.4, 4.4.1, 4.4.2,
        // you may use your own java script interface or other hybrid framework.

        // Android 5.0.1
        public boolean onShowFileChooser(
                WebView webView, ValueCallback<Uri[]> filePathCallback,
                FileChooserParams fileChooserParams) {

            String acceptTypes[] = fileChooserParams.getAcceptTypes();

            String acceptType = "";
            for (int i = 0; i < acceptTypes.length; ++ i) {
                if (acceptTypes[i] != null && acceptTypes[i].length() != 0)
                    acceptType += acceptTypes[i] + ";";
            }
            if (acceptType.length() == 0)
                acceptType = "*/*";

            final ValueCallback<Uri[]> finalFilePathCallback = filePathCallback;

            ValueCallback<Uri> vc = new ValueCallback<Uri>() {

                @Override
                public void onReceiveValue(Uri value) {

                    Uri[] result;
                    if (value != null)
                        result = new Uri[]{value};
                    else
                        result = null;

                    finalFilePathCallback.onReceiveValue(result);

                }
            };

            openFileChooser(vc, acceptType, "filesystem");


            return true;
        }
    };

    class Controller {
        final static int FILE_SELECTED = 4;

        Activity getActivity() {
            return MainActivity.this;
        }
    }

    // copied from android-4.4.3_r1/src/com/android/browser/UploadHandler.java
    //////////////////////////////////////////////////////////////////////

    /*
     * Copyright (C) 2010 The Android Open Source Project
     *
     * Licensed under the Apache License, Version 2.0 (the "License");
     * you may not use this file except in compliance with the License.
     * You may obtain a copy of the License at
     *
     *      http://www.apache.org/licenses/LICENSE-2.0
     *
     * Unless required by applicable law or agreed to in writing, software
     * distributed under the License is distributed on an "AS IS" BASIS,
     * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     * See the License for the specific language governing permissions and
     * limitations under the License.
     */

    // package com.android.browser;
    //
    // import android.app.Activity;
    // import android.content.ActivityNotFoundException;
    // import android.content.Intent;
    // import android.net.Uri;
    // import android.os.Environment;
    // import android.provider.MediaStore;
    // import android.webkit.ValueCallback;
    // import android.widget.Toast;
    //
    // import java.io.File;
    // import java.util.Vector;
    //
    // /**
    // * Handle the file upload callbacks from WebView here
    // */
    // public class UploadHandler {

    class UploadHandler {
        /*
         * The Object used to inform the WebView of the file to upload.
         */
        private ValueCallback<Uri> mUploadMessage;
        private String mCameraFilePath;
        private boolean mHandled;
        private boolean mCaughtActivityNotFoundException;
        private Controller mController;
        public UploadHandler(Controller controller) {
            mController = controller;
        }
        String getFilePath() {
            return mCameraFilePath;
        }
        boolean handled() {
            return mHandled;
        }
        void onResult(int resultCode, Intent intent) {
            if (resultCode == Activity.RESULT_CANCELED && mCaughtActivityNotFoundException) {
                // Couldn't resolve an activity, we are going to try again so skip
                // this result.
                mCaughtActivityNotFoundException = false;
                return;
            }
            Uri result = intent == null || resultCode != Activity.RESULT_OK ? null
                    : intent.getData();
            // As we ask the camera to save the result of the user taking
            // a picture, the camera application does not return anything other
            // than RESULT_OK. So we need to check whether the file we expected
            // was written to disk in the in the case that we
            // did not get an intent returned but did get a RESULT_OK. If it was,
            // we assume that this result has came back from the camera.
            if (result == null && intent == null && resultCode == Activity.RESULT_OK) {
                File cameraFile = new File(mCameraFilePath);
                if (cameraFile.exists()) {
                    result = Uri.fromFile(cameraFile);
                    // Broadcast to the media scanner that we have a new photo
                    // so it will be added into the gallery for the user.
                    mController.getActivity().sendBroadcast(
                            new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, result));
                }
            }
            mUploadMessage.onReceiveValue(result);
            mHandled = true;
            mCaughtActivityNotFoundException = false;
        }
        void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
            final String imageMimeType = "image/*";
            final String videoMimeType = "video/*";
            final String audioMimeType = "audio/*";
            final String mediaSourceKey = "capture";
            final String mediaSourceValueCamera = "camera";
            final String mediaSourceValueFileSystem = "filesystem";
            final String mediaSourceValueCamcorder = "camcorder";
            final String mediaSourceValueMicrophone = "microphone";
            // According to the spec, media source can be 'filesystem' or 'camera' or 'camcorder'
            // or 'microphone' and the default value should be 'filesystem'.
            String mediaSource = mediaSourceValueFileSystem;
            if (mUploadMessage != null) {
                // Already a file picker operation in progress.
                return;
            }
            mUploadMessage = uploadMsg;
            // Parse the accept type.
            String params[] = acceptType.split(";");
            String mimeType = params[0];
            if (capture.length() > 0) {
                mediaSource = capture;
            }
            if (capture.equals(mediaSourceValueFileSystem)) {
                // To maintain backwards compatibility with the previous implementation
                // of the media capture API, if the value of the 'capture' attribute is
                // "filesystem", we should examine the accept-type for a MIME type that
                // may specify a different capture value.
                for (String p : params) {
                    String[] keyValue = p.split("=");
                    if (keyValue.length == 2) {
                        // Process key=value parameters.
                        if (mediaSourceKey.equals(keyValue[0])) {
                            mediaSource = keyValue[1];
                        }
                    }
                }
            }
            //Ensure it is not still set from a previous upload.
            mCameraFilePath = null;
            if (mimeType.equals(imageMimeType)) {
                if (mediaSource.equals(mediaSourceValueCamera)) {
                    // Specified 'image/*' and requested the camera, so go ahead and launch the
                    // camera directly.
                    startActivity(createCameraIntent());
                    return;
                } else {
                    // Specified just 'image/*', capture=filesystem, or an invalid capture parameter.
                    // In all these cases we show a traditional picker filetered on accept type
                    // so launch an intent for both the Camera and image/* OPENABLE.
                    Intent chooser = createChooserIntent(createCameraIntent());
                    chooser.putExtra(Intent.EXTRA_INTENT, createOpenableIntent(imageMimeType));
                    startActivity(chooser);
                    return;
                }
            } else if (mimeType.equals(videoMimeType)) {
                if (mediaSource.equals(mediaSourceValueCamcorder)) {
                    // Specified 'video/*' and requested the camcorder, so go ahead and launch the
                    // camcorder directly.
                    startActivity(createCamcorderIntent());
                    return;
                } else {
                    // Specified just 'video/*', capture=filesystem or an invalid capture parameter.
                    // In all these cases we show an intent for the traditional file picker, filtered
                    // on accept type so launch an intent for both camcorder and video/* OPENABLE.
                    Intent chooser = createChooserIntent(createCamcorderIntent());
                    chooser.putExtra(Intent.EXTRA_INTENT, createOpenableIntent(videoMimeType));
                    startActivity(chooser);
                    return;
                }
            } else if (mimeType.equals(audioMimeType)) {
                if (mediaSource.equals(mediaSourceValueMicrophone)) {
                    // Specified 'audio/*' and requested microphone, so go ahead and launch the sound
                    // recorder.
                    startActivity(createSoundRecorderIntent());
                    return;
                } else {
                    // Specified just 'audio/*',  capture=filesystem of an invalid capture parameter.
                    // In all these cases so go ahead and launch an intent for both the sound
                    // recorder and audio/* OPENABLE.
                    Intent chooser = createChooserIntent(createSoundRecorderIntent());
                    chooser.putExtra(Intent.EXTRA_INTENT, createOpenableIntent(audioMimeType));
                    startActivity(chooser);
                    return;
                }
            }
            // No special handling based on the accept type was necessary, so trigger the default
            // file upload chooser.
            startActivity(createDefaultOpenableIntent());
        }
        private void startActivity(Intent intent) {
            try {
                mController.getActivity().startActivityForResult(intent, Controller.FILE_SELECTED);
            } catch (ActivityNotFoundException e) {
                // No installed app was able to handle the intent that
                // we sent, so fallback to the default file upload control.
                try {
                    mCaughtActivityNotFoundException = true;
                    mController.getActivity().startActivityForResult(createDefaultOpenableIntent(),
                            Controller.FILE_SELECTED);
                } catch (ActivityNotFoundException e2) {
                    // Nothing can return us a file, so file upload is effectively disabled.
                    Toast.makeText(mController.getActivity(), R.string.uploads_disabled,
                            Toast.LENGTH_LONG).show();
                }
            }
        }
        private Intent createDefaultOpenableIntent() {
            // Create and return a chooser with the default OPENABLE
            // actions including the camera, camcorder and sound
            // recorder where available.
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("*/*");
            Intent chooser = createChooserIntent(createCameraIntent(), createCamcorderIntent(),
                    createSoundRecorderIntent());
            chooser.putExtra(Intent.EXTRA_INTENT, i);
            return chooser;
        }
        private Intent createChooserIntent(Intent... intents) {
            Intent chooser = new Intent(Intent.ACTION_CHOOSER);
            chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intents);
            chooser.putExtra(Intent.EXTRA_TITLE,
                    mController.getActivity().getResources()
                            .getString(R.string.choose_upload));
            return chooser;
        }
        private Intent createOpenableIntent(String type) {
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType(type);
            return i;
        }
        private Intent createCameraIntent() {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File externalDataDir = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DCIM);
            File cameraDataDir = new File(externalDataDir.getAbsolutePath() +
                    File.separator + "browser-photos");
            cameraDataDir.mkdirs();
            mCameraFilePath = cameraDataDir.getAbsolutePath() + File.separator +
                    System.currentTimeMillis() + ".jpg";
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(mCameraFilePath)));
            return cameraIntent;
        }
        private Intent createCamcorderIntent() {
            return new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        }
        private Intent createSoundRecorderIntent() {
            return new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
        }
    }
}



